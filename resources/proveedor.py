import logging
from flasgger import swag_from
from flask import request
from models.proveedor import ProveedorModel
from flask_restful import Resource, reqparse

from utils import paginated_results, restrict

class Proveedor(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id_proveedor', type = int)
    parser.add_argument('nombre', type = str)
    parser.add_argument('direccion', type = str)
    parser.add_argument('telefono', type = str)


    @swag_from('../swagger/proveedor/get_proveedor.yaml')
    def get(self, id_proveedor):
        proveedor = ProveedorModel.find_by_id(id_proveedor)
        if proveedor:
            return proveedor.json()

        return {'message': 'No se encuentra el Proveedor'}, 404

    @swag_from('../swagger/proveedor/put_proveedor.yaml')
    def put(self, id_proveedor):
        proveedor = ProveedorModel.find_by_id(id_proveedor)
        if proveedor:
            newdata = Proveedor.parser.parse_args()
            proveedor.from_reqparse(newdata)
            proveedor.save_to_db()
            return proveedor.json()

        return {'message': 'No se encuentra la Proveedor'}, 404
    
    @swag_from('../swagger/proveedor/delete_proveedor.yaml')
    def delete(self, id_proveedor):
        proveedor = ProveedorModel.find_by_id(id_proveedor)
        if proveedor:
            proveedor.delete_from_db()

        return {'message': 'Se ha borrado Proveedor'}
        
class ProveedorList(Resource):
    @swag_from('../swagger/proveedor/list_proveedor.yaml')
    def get(self):
        query = ProveedorModel.query
        return paginated_results(query)

    @swag_from('../swagger/proveedor/post_proveedor.yaml')
    def post(self):
        data = Proveedor.parser.parse_args()

        id_proveedor = data.get('id_proveedor')

        if id_proveedor is not None and ProveedorModel.find_by_id(id_proveedor):
            return {'message': "Ya existe un proveedor con el id_proveedor"}, 404
        
        proveedor = ProveedorModel(**data)
        try:
            proveedor.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error al crear el proveedor.', exc_info=e)
            return {'message': "Ocurrio un error al crear el proveedor"}, 500
        return proveedor.json(), 201

class ProveedorSearch(Resource):
    @swag_from('../swagger/proveedor/search_proveedor.yaml')
    def post(self):
        query = ProveedorModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'id_proveedor', lambda x: ProveedorModel.id_proveedor == x)
            query = restrict (query, filters,'nombre', lambda x: ProveedorModel.nombre.contains(x))
            query = restrict (query, filters,'direccion', lambda x: ProveedorModel.direccion == x)
            query = restrict (query, filters,'telefono', lambda x: ProveedorModel.telefono == x)
        return paginated_results(query)