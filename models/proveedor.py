from flask_restful.reqparse import Namespace
from db import db

from utils import _assign_if_something

class ProveedorModel(db.Model):
    __tablename__ = 'proveedor'

    id_proveedor = db.Column(db.Integer, primary_key = True)
    nombre = db.Column(db.String)
    direccion = db.Column(db.String)
    telefono = db.Column(db.String)

    def __init__(self, id_proveedor, nombre, direccion, telefono):
        self.id_proveedor = id_proveedor
        self.nombre = nombre
        self.direccion = direccion
        self.telefono = telefono
    
    def json(self, jsondepth = 0):
        json = {
            'id_proveedor': self.id_proveedor,
            'nombre': self.nombre,
            'direccion': self.direccion,
            'telefono': self.telefono
        }

        return json

    @classmethod
    def find_by_id(cls, id_proveedor):
        return cls.query.filter_by(id_proveedor = id_proveedor).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def from_reqparse(self, newdata: Namespace):
        for no_pk_key in ['nombre','direccion','telefono']:
            _assign_if_something(self, newdata, no_pk_key)