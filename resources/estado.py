import logging
from flasgger import swag_from
from flask import request
from models.estado import EstadoModel
from flask_restful import Resource, reqparse

from utils import paginated_results, restrict

class Estado(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id_estado', type = int)
    parser.add_argument('nombre_estado', type = str)

    @swag_from('../swagger/estado/get_estado.yaml')
    def get(self, id_estado):
        estado = EstadoModel.find_by_id(id_estado)
        if estado:
            return estado.json()

        return {'message': 'No se encuentra el estado'}, 404

    @swag_from('../swagger/estado/put_estado.yaml')
    def put(self, id_estado):
        estado = EstadoModel.find_by_id(id_estado)
        if estado:
            newdata = Estado.parser.parse_args()
            estado.from_reqparse(newdata)
            estado.save_to_db()
            return estado.json()

        return {'message': 'No se encuentra el estado'}, 404
    
    @swag_from('../swagger/estado/delete_estado.yaml')
    def delete(self, id_estado):
        estado = EstadoModel.find_by_id(id_estado)
        if estado:
            estado.delete_from_db()

        return {'message': 'Se ha borrado el estado'}
        
class EstadoList(Resource):
    @swag_from('../swagger/estado/list_estado.yaml')
    def get(self):
        query = EstadoModel.query
        return paginated_results(query)

    @swag_from('../swagger/estado/post_estado.yaml')
    def post(self):
        data = Estado.parser.parse_args()

        id_estado = data.get('id_estado')

        if id_estado is not None and EstadoModel.find_by_id(id_estado):
            return {'message': "Ya existe un estado con el id_estado"}, 404
        
        estado = EstadoModel(**data)
        try:
            estado.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error al crear el estado.', exc_info=e)
            return {'message': "Ocurrio un error al crear el estado"}, 500
        return estado.json(), 201

class EstadoSearch(Resource):
    @swag_from('../swagger/estado/search_estado.yaml')
    def post(self):
        query = EstadoModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'id_estado', lambda x: EstadoModel.id_estado == x)
            query = restrict (query, filters,'nombre_estado', lambda x: EstadoModel.nombre_estado.contains(x))
        return paginated_results(query)