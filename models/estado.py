from flask_restful.reqparse import Namespace
from db import db

from utils import _assign_if_something

class EstadoModel(db.Model):
    __tablename__ = 'estado'

    id_estado = db.Column(db.Integer, primary_key = True)
    nombre_estado = db.Column(db.String)
    
    def __init__(self, id_estado, nombre_estado):
        self.id_estado = id_estado
        self.nombre_estado = nombre_estado
    
    def json(self, jsondepth = 0):
        json = {
            'id_estado': self.id_estado,
            'nombre_estado': self.nombre_estado,
        }
        return json

    @classmethod
    def find_by_id(cls, id_estado):
        return cls.query.filter_by(id_estado = id_estado).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def from_reqparse(self, newdata: Namespace):
        for no_pk_key in ['nombre_estado']:
            _assign_if_something(self, newdata, no_pk_key)