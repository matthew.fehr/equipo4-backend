import logging
from flasgger import swag_from
from flask import request
from models.producto import ProductoModel
from flask_restful import Resource, reqparse

from utils import paginated_results, restrict

class Producto(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id_producto', type = int)
    parser.add_argument('nombre', type = str)
    parser.add_argument('descripcion', type = str)
    parser.add_argument('precio', type = int)
    parser.add_argument('id_estado', type = int)
    parser.add_argument('id_categoria', type = int)
    parser.add_argument('id_proveedor', type = int)


    @swag_from('../swagger/producto/get_producto.yaml')
    def get(self, id_producto):
        producto = ProductoModel.find_by_id(id_producto)
        if producto:
            return producto.json()

        return {'message': 'No se encuentra el producto'}, 404

    @swag_from('../swagger/producto/put_producto.yaml')
    def put(self, id_producto):
        producto = ProductoModel.find_by_id(id_producto)
        if producto:
            newdata = Producto.parser.parse_args()
            producto.from_reqparse(newdata)
            producto.save_to_db()
            return producto.json()

        return {'message': 'No se encuentra el producto'}, 404
    
    @swag_from('../swagger/producto/delete_producto.yaml')
    def delete(self, id_producto):
        producto = ProductoModel.find_by_id(id_producto)
        if producto:
            producto.delete_from_db()

        return {'message': 'Se ha borrado el producto'}
        
class ProductoList(Resource):
    @swag_from('../swagger/producto/list_producto.yaml')
    def get(self):
        query = ProductoModel.query
        return paginated_results(query)

    @swag_from('../swagger/producto/post_producto.yaml')
    def post(self):
        data = Producto.parser.parse_args()

        id_producto = data.get('id_producto')

        if id_producto is not None and ProductoModel.find_by_id(id_producto):
            return {'message': "Ya existe un producto con el id_producto"}, 404
        
        producto = ProductoModel(**data)
        try:
            producto.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error al crear el producto.', exc_info=e)
            return {'message': "Ocurrio un error al crear el producto"}, 500
        return producto.json(), 201

class ProductoSearch(Resource):
    @swag_from('../swagger/producto/search_producto.yaml')
    def post(self):
        query = ProductoModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'id_producto', lambda x: ProductoModel.id_producto == x)
            query = restrict (query, filters,'nombre', lambda x: ProductoModel.nombre.contains(x))
            query = restrict (query, filters,'descripcion', lambda x: ProductoModel.descripcion == x)
            query = restrict (query, filters,'precio', lambda x: ProductoModel.precio == x)
            query = restrict (query, filters,'id_estado', lambda x: ProductoModel.precio == x)
            query = restrict (query, filters,'id_categoria', lambda x: ProductoModel.precio == x)
            query = restrict (query, filters,'id_proveedor', lambda x: ProductoModel.precio == x)
        return paginated_results(query)