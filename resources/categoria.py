import logging
from flasgger import swag_from
from flask import request
from models.categoria import CategoriaModel
from flask_restful import Resource, reqparse

from utils import paginated_results, restrict

class Categoria(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id_categoria', type = int)
    parser.add_argument('descripcion', type = str)

    @swag_from('../swagger/categoria/get_categoria.yaml')
    def get(self, id_categoria):
        categoria = CategoriaModel.find_by_id(id_categoria)
        if categoria:
            return categoria.json()

        return {'message': 'No se encuentra la categoria'}, 404

    @swag_from('../swagger/categoria/put_categoria.yaml')
    def put(self, id_categoria):
        categoria = CategoriaModel.find_by_id(id_categoria)
        if categoria:
            newdata = Categoria.parser.parse_args()
            categoria.from_reqparse(newdata)
            categoria.save_to_db()
            return categoria.json()

        return {'message': 'No se encuentra la categoria'}, 404
    
    @swag_from('../swagger/categoria/delete_categoria.yaml')
    def delete(self, id_categoria):
        categoria = CategoriaModel.find_by_id(id_categoria)
        if categoria:
            categoria.delete_from_db()

        return {'message': 'Se ha borrado la categoria'}
        
class CategoriaList(Resource):
    @swag_from('../swagger/categoria/list_categoria.yaml')
    def get(self):
        query = CategoriaModel.query
        return paginated_results(query)

    @swag_from('../swagger/categoria/post_categoria.yaml')
    def post(self):
        data = Categoria.parser.parse_args()

        id_categoria = data.get('id_categoria')

        if id_categoria is not None and CategoriaModel.find_by_id(id_categoria):
            return {'message': "Ya existe una categoria con el id_categoria"}, 404
        
        categoria = CategoriaModel(**data)
        try:
            categoria.save_to_db()
        except Exception as e:
            logging.error('Ocurrio un error al crear la categoria.', exc_info=e)
            return {'message': "Ocurrio un error al crear la categoria"}, 500
        return categoria.json(), 201

class CategoriaSearch(Resource):
    @swag_from('../swagger/categoria/search_categoria.yaml')
    def post(self):
        query = CategoriaModel.query
        if request.json:
            filters = request.json
            query = restrict (query, filters,'id_categoria', lambda x: CategoriaModel.id_categoria == x)
            query = restrict (query, filters,'descripcion', lambda x: CategoriaModel.descripcion.contains(x))
        return paginated_results(query)