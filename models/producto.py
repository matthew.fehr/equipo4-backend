from models.categoria import CategoriaModel
from models.estado import EstadoModel
from models.proveedor import ProveedorModel
from flask_restful.reqparse import Namespace
from db import db

from utils import _assign_if_something

class ProductoModel(db.Model):
    __tablename__ = 'producto'

    id_producto = db.Column(db.Integer, primary_key = True)
    nombre = db.Column(db.String)
    descripcion = db.Column(db.String)
    precio = db.Column(db.Integer)
    id_estado = db.Column(db.Integer, db.ForeignKey(EstadoModel.id_estado))
    id_categoria = db.Column(db.Integer, db.ForeignKey(CategoriaModel.id_categoria))
    id_proveedor = db.Column(db.Integer, db.ForeignKey(ProveedorModel.id_proveedor))

    _estado = db.relationship('EstadoModel', uselist=False, primaryjoin='EstadoModel.id_estado == ProductoModel.id_estado', foreign_keys='ProductoModel.id_estado')
    _categoria = db.relationship('CategoriaModel', uselist=False, primaryjoin='CategoriaModel.id_categoria == ProductoModel.id_categoria', foreign_keys='ProductoModel.id_categoria')
    _proveedor = db.relationship('ProveedorModel', uselist=False, primaryjoin='ProveedorModel.id_proveedor == ProductoModel.id_proveedor', foreign_keys='ProductoModel.id_proveedor')

    def __init__(self, id_producto, nombre, descripcion, precio,  id_estado, id_categoria, id_proveedor):
        self.id_producto = id_producto
        self.nombre = nombre
        self.descripcion = descripcion
        self.precio = precio
        self.id_estado = id_estado
        self.id_categoria = id_categoria
        self.id_proveedor = id_proveedor
    
    def json(self, jsondepth = 0):
        json = {
            'id_producto': self.id_producto,
            'nombre': self.nombre,
            'descripcion': self.descripcion,
            'precio': self.precio,
            'id_estado': self.id_estado,
            'id_categoria': self.id_categoria,
            'id_proveedor': self.id_proveedor
        }

        if jsondepth > 0:
            if self._estado:
                json['_estado'] = self._estado.json(jsondepth)

            if self._categoria:
                json['_categoria'] = self._categoria.json(jsondepth)

            if self._proveedor:
                json['_proveedor'] = self._proveedor.json(jsondepth)

        return json

    @classmethod
    def find_by_id(cls, id_producto):
        return cls.query.filter_by(id_producto = id_producto).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    def from_reqparse(self, newdata: Namespace):
        for no_pk_key in ['nombre','descripcion','precio','id_estado', 'id_categoria','id_proveedor']:
            _assign_if_something(self, newdata, no_pk_key)